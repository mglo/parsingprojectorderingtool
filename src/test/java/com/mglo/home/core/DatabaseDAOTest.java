package com.mglo.home.core;

import com.mglo.data.ClientOrder;
import com.mglo.data.Database;
import com.mglo.data.DatabaseDAO;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class DatabaseDAOTest {

    @Test
    public void addOneObjectFromDatabase_DAOmethod(){
        // given
        DatabaseDAO databaseDAO = new DatabaseDAO();
        List<ClientOrder> orderList = Database.getInstance();
        int firstListSize = orderList.size();

        // when
        databaseDAO.addOrder(new ClientOrder(1,1,"Chleb", 3, 4.30));

        // then
        assertEquals(firstListSize + 1, orderList.size());
    }

    @Test
    public void removeOneObjectfromDatabase_DAOmethod(){
        // given
        DatabaseDAO databaseDAO = new DatabaseDAO();
        List<ClientOrder> orderList = Database.getInstance();
        databaseDAO.addOrder(new ClientOrder(1,1,"Chleb", 3, 4.30));
        int firstListSize = orderList.size();

        // when
        databaseDAO.removeOrder(new ClientOrder(1,1,"Chleb", 3, 4.30));

        // then
        assertEquals(firstListSize - 1, orderList.size());

    }
}
