package com.mglo.home.core;

import com.mglo.data.ClientOrder;
import com.mglo.utils.ParsingXML;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class ParsingXMLTest {

    @Before
    public void beforeTests(){
        String testXML = "<requests><request><clientId>1</clientId><requestId>1</requestId><name>Bułka</name><quantity>1</quantity><price>10.00</price></request></requests>";
        try(BufferedWriter bw = Files.newBufferedWriter(Paths.get("data/test.xml"))) {
            bw.write(testXML);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testingReadListFromXmlFile(){
       //given
        List<ClientOrder> outputList = null;
        ClientOrder expectedObject = new ClientOrder(1,1,"Bułka",1,10.00);

        //when
        try {
            outputList = ParsingXML.readListFromXmlFile(new File("data/test.xml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //then
        assertTrue(expectedObject.equals(outputList.get(0)));
    }

    @After
    public void CleanupTests(){
        File file = new File("data/test.xml");
        try {
            file.delete();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
