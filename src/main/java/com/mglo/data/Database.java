package com.mglo.data;

import java.util.ArrayList;
import java.util.List;

public class Database {
    private static List<ClientOrder> clientOrders;

    private Database() {
        if(clientOrders != null){
            throw new IllegalArgumentException("cannot create new instance.");
        }
    }

    public static List<ClientOrder> getInstance(){
        if(clientOrders == null){
            clientOrders = new ArrayList<>();
        }
        return clientOrders;
    }

}
