package com.mglo.data;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ClientOrder {

    @JsonAlias("Client_Id")
    private int clientId;
    @JsonAlias("Request_id")
    private int requestId;
    @JsonAlias("Name")
    private String name;
    @JsonAlias("Quantity")
    private int quantity;
    @JsonAlias("Price")
    private double price;

    public ClientOrder() {
    }

    public ClientOrder(int clientId, int requestId, String name, int quantity, double price) {
        this.clientId = clientId;
        this.requestId = requestId;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientOrder clientOrder = (ClientOrder) o;
        return clientId == clientOrder.clientId &&
                requestId == clientOrder.requestId &&
                quantity == clientOrder.quantity &&
                Double.compare(clientOrder.price, price) == 0 &&
                Objects.equals(name, clientOrder.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(clientId, requestId, name, quantity, price);
    }

    @Override
    public String toString() {
        return "ClientOrder{" +
                "clientId=" + clientId +
                ", requestId=" + requestId +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
