package com.mglo.data;

import java.util.List;

public class DatabaseDAO {

    List<ClientOrder> ordersDatabase;

    public void addOrder(ClientOrder clientOrder){
        ordersDatabase = Database.getInstance();
        ordersDatabase.add(clientOrder);
    }

    public boolean removeOrder(ClientOrder clientOrder){
        ordersDatabase = Database.getInstance();

        for (int i = 0; i < ordersDatabase.size(); i++) {
            //TODO what will happened when clientOrder is removed and continue. is still checking..
            if(ordersDatabase.get(i).equals(clientOrder)){
                ordersDatabase.remove(clientOrder);
                return true;
            }
        }
        return false;
    }

    public List<ClientOrder> getOrders(){
        //TODO test this method
        ordersDatabase = Database.getInstance();
        int size = ordersDatabase.size();

        if (size > 0 ) {
            return ordersDatabase;
        }
        return null;
    }

    public ClientOrder getOrder(int indexNumber){
        //TODO test this method
        ordersDatabase = Database.getInstance();
        int size = ordersDatabase.size();

        if (size > 0 && indexNumber >= 0 && indexNumber <= size) {
           return ordersDatabase.get(indexNumber);
        }
        return null;
    }

    //TODO write tests for this code
}
