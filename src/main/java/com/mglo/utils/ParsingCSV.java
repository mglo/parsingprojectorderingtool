package com.mglo.utils;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.mglo.data.ClientOrder;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ParsingCSV {

    public static List<ClientOrder> readFromCSVFile(File file) throws IOException {
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema withHeaderHeaderSchema = CsvSchema.emptySchema().withHeader();

        MappingIterator<ClientOrder> it = csvMapper.readerFor(ClientOrder.class).with(withHeaderHeaderSchema).readValues(file);
        List<ClientOrder> clientOrdersListCSV = it.readAll();

        return clientOrdersListCSV;
    }


}
