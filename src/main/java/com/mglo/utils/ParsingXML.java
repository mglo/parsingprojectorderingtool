package com.mglo.utils;

import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.mglo.data.ClientOrder;

import java.io.*;
import java.util.List;

public class ParsingXML {

        public static List<ClientOrder> readListFromXmlFile(File file) throws IOException {
            XmlMapper xmlMapper = new XmlMapper();
            CollectionType typeReference = TypeFactory.defaultInstance().constructCollectionType(List.class, ClientOrder.class);
            List<ClientOrder> clientOrdersList = xmlMapper.readValue(file, typeReference);

            return clientOrdersList;
        }

}
