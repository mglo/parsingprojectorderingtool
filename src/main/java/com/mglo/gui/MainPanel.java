package com.mglo.gui;

import javax.swing.*;

public class MainPanel extends JPanel {
    public static final String SELECT_BUTTON = "UPLOAD";

    private JButton selectButton;

    public MainPanel() {
        selectButton = new JButton(SELECT_BUTTON);
        add(selectButton);
    }

    public JButton getSelectButton() {
        return selectButton;
    }
}
