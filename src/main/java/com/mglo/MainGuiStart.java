package com.mglo;

import com.mglo.data.ClientOrder;
import com.mglo.data.DatabaseDAO;
import com.mglo.externalData.PickAFile;
import com.mglo.utils.ParsingCSV;
import com.mglo.utils.ParsingXML;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainGuiStart {
    private JButton buttonImport;
    private JButton buttonExport;
    private JComboBox comboBoxRequestsList;
    private JButton createButton;
    private JPanel panelMain;
    private JTextField textFieldClientNumber;
    private JLabel labelSelectReport;
    private JLabel labelClientID;
    private JTextArea textArea1;
    DatabaseDAO databaseDAO = new DatabaseDAO();

    private String[] reportTypes = {
            "Ilosc zamówien łącznie",
            "Ilość zamówień do klienta: ",
            "łączna kwota zamówień",
            "łączna kwota zamówien do klienta: ",
            "Lista wszystkich zamówień",
            "Lista zamówień do klienta: ",
            "Średnia wartość zamówienia",
            "średnia wartość zamówienia do klienta: "
    };

    public MainGuiStart() {
        startContent();
        buttonImport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File file = PickAFile.selectFile();

                if (file != null) {
                    int nameLength = file.getName().length();
                    String extension = file.getName().substring(nameLength - 3, nameLength);

                    if (extension.equals("csv")) {
                        //System.out.println("CSV file was selected");
                        try {
                            List<ClientOrder> ordersCSV = ParsingCSV.readFromCSVFile(file);
                            if(ordersCSV != null) {
                                for (int i = 0; i < ordersCSV.size(); i++) {
                                    databaseDAO.addOrder(ordersCSV.get(i));
                                }
                                JOptionPane.showMessageDialog(null, "CSV File imported with success!", "Message", JOptionPane.INFORMATION_MESSAGE);
                            }else{
                                JOptionPane.showMessageDialog(null, "Sorry, CSV File was not imported", "Message", JOptionPane.INFORMATION_MESSAGE);
                            }
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }

                    } else if (extension.equals("xml")) {
                        //System.out.println("XML file was selected");
                        try {
                            List<ClientOrder> ordersXML = ParsingXML.readListFromXmlFile(file);
                            if(ordersXML != null) {
                                for (int i = 0; i < ordersXML.size(); i++) {
                                    databaseDAO.addOrder(ordersXML.get(i));
                                }
                                JOptionPane.showMessageDialog(null, "XML File imported with success!", "Message", JOptionPane.INFORMATION_MESSAGE);
                            }else{
                                JOptionPane.showMessageDialog(null, "Sorry, XML File was not imported", "Message", JOptionPane.INFORMATION_MESSAGE);
                            }
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    } else {
                        System.out.println("Wrong file type was selected.");
                    }
                }
            }
        });
        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                manageAction(comboBoxRequestsList.getSelectedItem().toString());

            }
        });
        buttonExport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Exporting stuff");
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Report generator tool 1.0");
        frame.setContentPane(new MainGuiStart().panelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void startContent(){
        panelMain = new JPanel();
        panelMain.setLayout(new BorderLayout(0, 0));
        panelMain.setMinimumSize(new Dimension(600, 400));
        panelMain.setPreferredSize(new Dimension(600, 400));

        final JPanel panel1 = new JPanel();
        panel1.setLayout(new BorderLayout(0, 0));
        panelMain.add(panel1, BorderLayout.CENTER);

        final JPanel panel2 = new JPanel();
        panel2.setLayout(new BorderLayout(0, 0));
        panel1.add(panel2, BorderLayout.NORTH);

        final JPanel panel3 = new JPanel();
        panel3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        panel3.setPreferredSize(new Dimension(100, 38));
        panel2.add(panel3, BorderLayout.CENTER);

        labelSelectReport = new JLabel();
        labelSelectReport.setText("Select Report: ");
        panel3.add(labelSelectReport);

        comboBoxRequestsList = new JComboBox(reportTypes);
        comboBoxRequestsList.setPreferredSize(new Dimension(250, 24));
        panel3.add(comboBoxRequestsList);

        labelClientID = new JLabel();
        labelClientID.setText("Client ID:");
        panel3.add(labelClientID);

        textFieldClientNumber = new JTextField();
        textFieldClientNumber.setColumns(3);
        panel3.add(textFieldClientNumber);

        createButton = new JButton();
        createButton.setText("Generate");
        panel3.add(createButton);

        final JPanel panel4 = new JPanel();
        panel4.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        panel1.add(panel4, BorderLayout.SOUTH);

        buttonImport = new JButton();
        buttonImport.setText("Import");
        panel4.add(buttonImport);

        buttonExport = new JButton();
        buttonExport.setText("Export");
        panel4.add(buttonExport);

        final JPanel panel5 = new JPanel();
        panel5.setLayout(new BorderLayout(0, 0));
        panel1.add(panel5, BorderLayout.CENTER);
        final JScrollPane scrollPane1 = new JScrollPane();
        panel5.add(scrollPane1, BorderLayout.CENTER);

        textArea1 = new JTextArea();
        scrollPane1.setViewportView(textArea1);
    }

    public void printOrdersInTextArea(List<ClientOrder> orders){
        textArea1.setText("");
        textArea1.setColumns(5);
        if(orders != null) {
            for (ClientOrder order : orders) {
                textArea1.append(order.toString());
                textArea1.append("\n");
            }
        }else{
            JOptionPane.showMessageDialog(null, "Please import orders.", "Message", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void printTextinTextArea(String textToPrint){
        textArea1.setText("");
        textArea1.setText(textToPrint);
    }


    public void manageAction(String action) {
        switch (action) {
            case "Ilosc zamówien łącznie":
                //printOrdersInTextArea(databaseDAO.getOrders());
                printTextinTextArea("Ilosc zamowien lacznie: " + AllOrdersNumber());
                break;
            case "Ilość zamówień do klienta: ":
                if(!textFieldClientNumber.getText().equals("")) {
                    int clientID = Integer.valueOf(textFieldClientNumber.getText());
                    int clientOrdersNumber = ordersFromClientByID(clientID);

                    if(clientOrdersNumber >= 0 && clientID >= 0) {
                        printTextinTextArea("Ilość zamówień do klienta numer " + clientID + " wynosi " + clientOrdersNumber);
                    }else{
                        printTextinTextArea("Klient numer " + clientID + " nie złożył zadnego zamowienia. ");
                    }
                }
                break;
            case "łączna kwota zamówień":
                break;
            case "łączna kwota zamówien do klienta: ":
                break;
            case "Lista wszystkich zamówień":
                break;
            case "Lista zamówień do klienta: ":
                break;
            case "Średnia wartość zamówienia":
                break;
            case "średnia wartość zamówienia do klienta: ":
                break;
        }
    }

    public int ordersFromClientByID(int ClientNumberText){
        if (ClientNumberText >= 0) {
            Set<Integer> requestsNumbers = new HashSet<>();

            for (int i = 0; i < databaseDAO.getOrders().size(); i++) {
                if (databaseDAO.getOrder(i).getClientId() == ClientNumberText) {
                    requestsNumbers.add(databaseDAO.getOrder(i).getRequestId());
                }
            }
            if (requestsNumbers.size() > 0) {
                return requestsNumbers.size();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Client ID is missing. Please provide before generating this report", "Message", JOptionPane.INFORMATION_MESSAGE);
            return -1;
        }
        return -1;
    }

    public int AllOrdersNumber() {
        if (databaseDAO.getOrders() != null) {
            Set<Integer> requestsNumbers = new HashSet<>();

            for (int i = 0; i < databaseDAO.getOrders().size(); i++) {
                requestsNumbers.add(databaseDAO.getOrder(i).getRequestId());
            }
            if (requestsNumbers.size() > 0) {
                return requestsNumbers.size();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Nie znaleziono zadnego zamowienia", "Message", JOptionPane.INFORMATION_MESSAGE);
            return -1;
        }
        return -1;
    }


}
